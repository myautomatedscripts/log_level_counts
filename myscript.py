import sys
from datetime import datetime
from collections import Counter
import collections
from datetime import date, timedelta


#capturing parameters
start_date = datetime.strptime(sys.argv[1], "%Y-%m-%d")
end_date = datetime.strptime(sys.argv[2], "%Y-%m-%d")
service_name = sys.argv[3]
log_type = sys.argv[4]

list_of_file_names = []
log_msg_dic = {'ERROR': '[ERROR]', 'INFO': '[INFO]', 'DEBUG': '[DEBUG]', 'FATAL': '[FATAL]'}

try:
  if start_date <= end_date:
    delta = end_date - start_date
    for i in range(delta.days + 1):
      day = start_date + timedelta(days=i)
      file_name = 'logs/' + day.strftime('%Y/%m/%d') + '/' + service_name + '-' + log_type + '.log'
      list_of_file_names.append(file_name)
  else:
    raise ValueError("Error: The second date input is earlier than the first one")
except ValueError:
  raise ValueError("Error: Incorrect format given for dates. They must be given like 'yyyy-mm-dd' (ex: '2020-08-01').") 


counts = Counter(dict.fromkeys(log_msg_dic.values(), 0))
for file in list_of_file_names:
    with open(file) as f:
        for words in map(str.split, f):
          counts.update(counts.keys() & words)
print(counts)